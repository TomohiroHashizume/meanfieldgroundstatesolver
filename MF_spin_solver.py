import numpy as np
import basis_mat as bm

import time 
import sys,os


def grab_choice(val,prob,numtraj) :
   return np.random.choice(val,size=(numtraj),p=prob) 

def normalize_state(state) :
   for ind in range(state.shape[0]) :
      state[ind,:] = state[ind,:]/np.sqrt(sum(state[ind,:]*np.conj(state[ind,:])))
   return state

def lisnpacenostart(Tf,steps) :
   arr,step=np.linspace(0,Tf,steps,endpoint=False,retstep=True)
   return (arr+step)

class MF_spin_solver :
   def __init__(self,spin=2,verbose=False) :
      self.verbose=verbose
      self.spin=2
      self.spin=spin

      self.locdim=int(self.spin*2+1)
      self.physicalmats=bm.spinmat(self.spin)
      self.Sx=self.physicalmats['Sx']
      self.Sy=self.physicalmats['Sy']
      self.Sz=self.physicalmats['Sz']
      self.Sp=self.physicalmats['Sp']
      self.Sm=self.physicalmats['Sm']
      self.I =self.physicalmats['I']

   def set_initial(self,state_in,seed=1,normalize=False) :
      self.seed=seed
      np.random.seed(self.seed)
      self.physicalstate=state_in
      self.N=self.physicalstate.shape[0] #number of sites are fixed from the initial state.
         # Length of the chain is defined from
         # The length of the initial state

      if (state_in.shape[1] != int(self.spin*2+1)) :
         print('input dimension does not match')
         sys.exit(0)

      if(normalize) :
         normalize_state(self.physicalstate)

      self.h_x_mean=np.zeros(self.N,dtype=np.complex128)
      self.h_y_mean=np.zeros(self.N,dtype=np.complex128)
      self.h_z_mean=np.zeros(self.N,dtype=np.complex128)

      for site in range(self.N) :
         state=self.physicalstate[site]
         self.h_x_mean[site] = np.conj(np.transpose(state))@self.Sx@state
         self.h_y_mean[site] = np.conj(np.transpose(state))@self.Sy@state
         self.h_z_mean[site] = np.conj(np.transpose(state))@self.Sz@state

   def set_Hamil(self,h_x,h_y,h_z,J_x,J_y,J_z):
      self.h_x=h_x
      self.h_y=h_y
      self.h_z=h_z

      self.J_x=J_x
      self.J_y=J_y
      self.J_z=J_z

   def solve(self,tol=1e-4,outdir='./') :
      self.tol=tol
      self.outdir=outdir
      self.statenow=self.physicalstate
      self.fname=self.__generate_fname()

      self.tolnow = np.Inf
      self.Hprev=np.inf
      self.Enow=self.sweep()

      while self.tolnow > self.tol :
         self.E_prev=self.Enow
         self.Enow=self.sweep()
         self.tolnow=np.abs(self.E_prev-self.Enow) 
         print(self.tolnow,self.Enow)
      self.statenow

   def sweep(self) :
      H=0.
      for site in range(self.N) :
         H += self.solve_local(site)
      return H

   def solve_local(self,site) :
      H_local=self.make_Hamil(site)
      E_local,v = np.linalg.eig(H_local)

      self.statenow[site]=v[:,0]

      self.h_x_mean[site]= np.conj(np.transpose(v[:,0]))@self.Sx@v[:,0]
      self.h_y_mean[site]= np.conj(np.transpose(v[:,0]))@self.Sy@v[:,0]
      self.h_z_mean[site]= np.conj(np.transpose(v[:,0]))@self.Sz@v[:,0]

      return E_local[0]


   def make_Hamil(self,site) :
      Hamil=0
      Hamil+=self.Sx*self.h_x[site]
      Hamil+=self.Sy*self.h_y[site]
      Hamil+=self.Sz*self.h_z[site]

      for site2 in range(self.N) :
         if site2 != site :
            Hamil+=self.J_x[site,site2]*self.h_x_mean[site2]*self.Sx
            Hamil+=self.J_y[site,site2]*self.h_y_mean[site2]*self.Sy
            Hamil+=self.J_z[site,site2]*self.h_z_mean[site2]*self.Sz
         else  :
            Hamil+=self.J_x[site,site2]*(self.Sx@self.Sx)
            Hamil+=self.J_y[site,site2]*(self.Sy@self.Sy)
            Hamil+=self.J_z[site,site2]*(self.Sz@self.Sz)

      return Hamil

   def __generate_fname(self) :
      return self.outdir+"/out_L_{0:d}_S_{1:1.1f}_seed_{2}_tol_{3:.3e}".format(self.N,self.spin,self.seed,self.tol)

   # Debug Functions: It doesn't mean that they are obsolete.
   ##########################################################
