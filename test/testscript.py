import sys,os
import numpy as np
import pathlib

import matplotlib.pyplot as plt

sys.path.append('../')
import MF_bh_solver as mf

nbosons = 10
print('Soft expansion dynamics of soft-core bosons on Quasi-crystalline lattice')
print('with maximum {0:d} bosons per site'.format(nbosons))

verbose=True
verbose=False
mf_test=mf.MF_bh_solver(nbosons,verbose=verbose)


betaparamstr="1onsqrt2"
if betaparamstr == "1onsqrt2" :
   betaparam="1/np.sqrt(2)"
else :
   print("error: betaparam corresponds to",betaparamstr,"does not exist!")
   sys.exit(0)

Lx=20
lambda_potonU=2.
lambda_potonU=0.
muonU=0.5

beta_pot=eval(betaparam)

J=1.0

Ly=Lx
L=Lx*Ly

ops=[]

first=1
last=20+1

J=1.
JonUmax=0.2

for JonU in range(first,last) :
   print(JonU)
   U=J/(JonUmax*JonU/(last-1))
   mu=np.abs(U)*muonU
   lambda_pot=lambda_potonU*np.abs(U)

   print('U={0:.2f}, J={1:.2f}, J/U={2:.4f}'.format(U,J,J/U))
   print('lambda={0:.4f}, beta={1:.4f} ({2}), mu={3:.4f}'.format(lambda_pot,beta_pot,betaparam,mu))
   print('Lx={0:d}, Ly={0:d}'.format(Lx,Ly))

   mf_test.set_initial(L)

   h_n=-J*mu*np.ones(L,dtype=np.complex128)

   for indi in range(L) :
      m = indi % Lx
      n = indi // Ly
      h_n[indi] += -J*lambda_pot*(np.cos(2*np.pi*beta_pot*(n+m))+np.cos(2*np.pi*beta_pot*(n-m)))

   h_n = h_n - (U/2.)*np.ones(L,dtype=np.complex128)

   J_t=np.zeros((L,L),dtype=np.complex128)

   for indi in range(L) :
      m1 = indi % Lx
      n1 = indi // Ly

      for indj in range(L) :
         m2 = indj %  Lx
         n2 = indj // Ly

         if (abs(m2-m1)==1) and (n2==n1) :
            J_t[indi,indj] = 1./2.
         elif (m2==m1) and (abs(n2-n1)==1) :
            J_t[indi,indj] = 1./2.

   J_t *= -J
   U_int = (U/2.)*(np.zeros((L,L),dtype=np.complex128) + np.diag(np.ones(L,dtype=np.complex128))) # onsite

   mf_test.set_Hamil(h_n,J_t,U_int)
   ops.append(mf_test.solve(1e-4))

plt.plot(JonUmax*(np.arange(first,last))/float(last),np.abs(np.array(ops)))
plt.show()
