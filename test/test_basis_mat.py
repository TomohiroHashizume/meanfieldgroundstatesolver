import sys,os
sys.path.append('../')
import numpy as np
import pathlib

import basis_mat as bm

maxn=10
locdim=int(maxn+1)
physicalmats=bm.bosonmat(maxn)
b_d =physicalmats['b_d']
b__ =physicalmats['b__']
n__ =physicalmats['n__']
I =physicalmats['I']

state=np.zeros(locdim)
state[-1]=1

print(b_d@state)
print(b__@state)

state=np.zeros(locdim)
state[-2]=1

print(b_d@state)
print(b__@state)

