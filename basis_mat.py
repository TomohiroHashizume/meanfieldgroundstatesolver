import numpy as np
def spinmat(spin) :
   dim = int(2*spin + 1)
   Sx = np.zeros((dim,dim),dtype=np.complex128)
   Sy = np.zeros((dim,dim),dtype=np.complex128)
   Sz = np.zeros((dim,dim),dtype=np.complex128)

   for m in range(0,dim) :
      for n in range(0,dim) :
         mt = - (m - spin)
         nt = - (n - spin)

         Sx[m,n] = ((mt==(nt+1))+((mt+1)==nt) )* (0.5) * np.sqrt(spin*(spin+1)-mt*nt)
         Sy[m,n] = ((mt==(nt+1))-((mt+1)==nt) )* (0.5) * np.sqrt(spin*(spin+1)-mt*nt)/1j

         Sz[m,n] = (mt==nt)*mt

   I = np.eye(dim)
   Sp=Sx+1j*Sy
   Sm=Sx-1j*Sy
   SpinMat={
         'Sx': Sx,
         'Sy': Sy,
         'Sz': Sz,
         'Sp': Sp,
         'Sm': Sm,
         'I' : I,
         }
   return SpinMat

def bosonmat(maxn) :
   dim = maxn+1
   b_d = np.zeros((dim,dim),dtype=np.complex128) + np.diag(np.sqrt(np.arange(maxn,0,-1,dtype=np.complex128)),k=1)
   b__ = np.zeros((dim,dim),dtype=np.complex128) + np.diag(np.sqrt(np.arange(maxn,0,-1,dtype=np.complex128)),k=-1)
   n__ = np.zeros((dim,dim),dtype=np.complex128) + np.diag(np.arange(maxn,-1,-1,dtype=np.complex128))

   I = np.eye(dim)

   SpinMat={
         'b_d': b_d,
         'b__': b__,
         'n__' : n__,
         'I' : I,
         }

   return SpinMat
