import numpy as np
import basis_mat as bm
import scipy.linalg as sl

import time 
import sys,os

def grab_choice(val,prob,numtraj) :
   return np.random.choice(val,size=(numtraj),p=prob) 

def normalize_state(state) :
   for ind in range(state.shape[0]) :
      state[ind,:] = state[ind,:]/np.sqrt(sum(state[ind,:]*np.conj(state[ind,:])))
   return state

def lisnpacenostart(Tf,steps) :
   arr,step=np.linspace(0,Tf,steps,endpoint=False,retstep=True)
   return (arr+step)

class MF_bh_solver :
   def __init__(self,maxn=4,verbose=False) :
      self.verbose=verbose

      self.maxn=maxn
      self.locdim=int(self.maxn+1)
      self.physicalmats=bm.bosonmat(self.maxn)
      self.b_d =self.physicalmats['b_d']
      self.b__ =self.physicalmats['b__']
      self.n__ =self.physicalmats['n__']
      self.I =self.physicalmats['I']

   def set_initial(self,N,seed=1,b_d_mean=[]) :
      self.seed=seed
      np.random.seed(self.seed)
      self.N=N
      self.G_state=np.zeros((self.N,self.locdim),dtype=np.complex128)

      if len(b_d_mean) < self.N :
         self.b_d_mean=1*np.ones(self.N,dtype=np.complex128)

      self.b___mean=np.conj(self.b_d_mean)
      self.n___mean=np.ones(self.N,dtype=np.complex128)

   def set_Hamil(self,h_n,J_t,U_int):
      self.h_n=h_n.copy()

      self.J_t=J_t.copy()
      self.U_int=U_int.copy()

   def solve(self,tol=1e-4,outdir='./') :
      self.tolpersite=tol
      self.tol=self.tolpersite/self.N
      self.outdir=outdir
      self.fname=self.__generate_fname()

      self.tolnow = np.Inf

      self.Bprev = np.Inf
      self.Eprev = np.Inf
      self.Enow=self.sweep()
      self.Bnow=np.sum(np.abs(self.b___mean) )

      self.pmax=1000
      self.p=0

      while self.tolnow > self.tol :
         if (self.verbose) :
            print('sweep',self.p,flush=True)

         self.Eprev=self.Enow
         self.Bprev=self.Bnow

         self.Enow=self.sweep()
         self.Bnow=np.sum(np.abs(self.b_d_mean))

         self.tolnow=np.abs(self.Bprev-self.Bnow) 

         self.p += 1
         if self.p > self.pmax :
            print('warning: exceeded the maximum number of sweep. phi=',np.sum(np.abs(self.b___mean)))
            break

      print('sum <b>=',np.sum(np.abs(self.b___mean)),'sum <n>=',np.sum(np.abs(self.n___mean)))
      np.savez(self.__generate_fname(),G_state=self.G_state)
      return np.sum(self.b___mean)

   def sweep(self) :
      H=0.
      for site in range(self.N) :
         H += self.solve_local(site)

      return H

   def solve_local(self,site) :
      H_local=self.make_Hamil(site)
      E_local,v = sl.eigh(H_local)

      idx=0

      self.G_state[site,:]=v[:,idx].copy()
      self.b_d_mean[site] = np.conj(np.transpose(v[:,idx]))@self.b_d@v[:,idx]
      self.b___mean[site] = np.conj(np.transpose(v[:,idx]))@self.b__@v[:,idx]
      self.n___mean[site] = np.conj(np.transpose(v[:,idx]))@self.n__@v[:,idx]

      return E_local[idx]

   def make_Hamil(self,site) :
      Hamil=0.
      Hamil += self.h_n[site]*self.n__

      for site2 in np.random.permutation(np.arange(self.N,dtype=int)):
         if site2 != site :
            Hamil+=        self.J_t[site,site2] *self.b_d_mean[site2]*self.b_d
            Hamil+=np.conj(self.J_t[site,site2])*self.b___mean[site2]*self.b__

         else :
            Hamil+=self.U_int[site,site2]*(self.n__@self.n__)

      return Hamil

   def __generate_fname(self) :
      return self.outdir+"/out_L_{0:d}_N_{1:1.1f}_seed_{2}_tol_{3:.3e}.npz".format(self.N,self.maxn,self.seed,self.tolpersite)

   def get_state(self) :
      return self.G_state 

   # Debug Functions: It doesn't mean that they are obsolete.
   ##########################################################
